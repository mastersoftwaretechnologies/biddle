$(document).ready(function(){
	$("#txtTitle").blur(function(){	  
         el = $(this);
    	  if(el.val().length < 50){
		$(".title-error").show();
		$('.title-error').delay(4000).fadeOut();
	}
	});

	$("#txtShortDesc").blur(function(){	  
         el = $(this);
    	  if(el.val().length < 100){
		$(".desc-error").show();
		$('.desc-error').delay(4000).fadeOut();
	}
	});


$("#txtUrl").blur(function(){	  
	el = $(this);
         var cl=".url-error";
	validateurl(el,cl);
});

function validateurl(value,cls) {
    	  if(value.val().length < 0){
		$(cls).show();
		$(cls).delay(4000).fadeOut();
	}
	else {
		var url=value.val();
		var rurl= /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;	
		if ((rurl.test(url))){
                       process='true';
			test = true;
                }
                 else {
		$(cls).show();
		$(cls).delay(4000).fadeOut();
		}				
	}
}

//empty checkbox on page reload
$('input[type=checkbox]').each(function() 
{ 
  this.checked = false; 
}); 

//show/hide check count and copy to clipboard button
$('input[type=checkbox]').click(function() 
{ 
  var totalCheckboxes = $('input:checkbox').length;
  var numberNotChecked = $('input:checkbox:not(":checked")').length
  var numchecked = totalCheckboxes-numberNotChecked;

  if(this.checked == true){
    $(".footer").css("height",'40px');
    $("button#cop").show();
    $(".zclip").show();
    $("span#checkcount").show();
    $("span#checkcount").text(numchecked + " links Checked");
	$(this).parent().parent().parent().addClass("active-search-bg");
  } 
  else{
    var numchecked = numchecked;

    if (numchecked == 0){
      $("button#cop").hide();
	  $(this).parent().parent().parent().removeClass("active-search-bg");
    }
    $("span#checkcount").text(numchecked + " links Checked");
  }
  if (totalCheckboxes == numberNotChecked ){
    $(".footer").css("height",'20px');
    $("button#cop").hide();
    $(".zclip").hide();
    $("span#checkcount").hide();
  }
}); 


$("button#cop").on('click', function (e) {  
  $("span.copied").show();
  $("span.copied").delay(4000).fadeOut();
})
//copy to clipboard functionality
$("button#cop").on('click', function (e) {	
  e.preventDefault();
}).each(function () {
  $("button#cop").on('mouseover', function(){

        //turn off this listening event for the element that triggered this
        $(this).off('mouseover');
    $(this).zclip({
   		path: 'http://www.steamdev.com/zclip/js/ZeroClipboard.swf',
   		copy: function() {
        var str = '';
        $('input:checked.copy-txt').each(function () {
          var key = $(this).parent().prev().parent().children("ul").children("li").children("a");
          if (key.length > 0){
            var keys = '';
            $.each(key,function(i,value){
               keys += key[i].innerHTML+','
            });
          }
          keys = keys.replace(/,([^,]*)$/,'$1'); //remove ',' after last keyword
          str += "URL: "+ $(this).parent().prev().parent().children("h4").text()+"\n";
          str += $(this).parent().prev().parent().children("p#pFull").text()+"\n";
          if($(this).parent().prev().parent().children("p").children("a").text() != ''){
          str += "GitHub Url: "+ $(this).parent().prev().parent().children("p").children("a").text()+"\n\n";
      }else{
        str +="\n\n";
      }
      });
      return (str);
    }
  });
	});
});
});

$(document).ready(function(){
    $(".tooltip").tooltip({
        placement : 'top'
    });
});
   