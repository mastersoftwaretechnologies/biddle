// controllers for biddle app       
function bidCtrl($scope,$http){
    $http({method: 'POST', url: '/reportbid'}).
    	success(function(data, status, headers, config) {
    	$scope.biddingreport=data;
    }).
        error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });
	$scope.bid = {
        BidderName: '',
        JobUrl: '',
        protocol:'',
        host:'',
        Hostname: '',
        hash:'',
        search:'',
        path:'',
        parameters:'',
        JobId:'',
        JobPortal: '',
        Status: '',
        Isinvite: '',
        Comments: '',

    };

    // directive to change job portal on filling the job url automatically
    $scope.changeName = function() {
        var url = $scope.url;
        var containsodesk = (url.indexOf('odesk') > -1); //true
        var containselance = (url.indexOf('elance') > -1); //tru
    if (containsodesk){
         $scope.name = 'odesk';
    }
    else if (containselance) {
        $scope.name = 'elance';
    }
    else{
        $scope.name = '';
    } 
    };

    $scope.status = [
        { name: 'Applying', value: 'Applying' }, 
        { name: 'Applied', value: 'Applied' }, 
    ];
    
    $scope.workstat = $scope.status[0].value ;
        //  save the new bid to the database
        $scope.createBid = function() {
            var bid = $scope.bid;
            var newbids = [];
            var biddername=$scope.biddername;
            var joburl=$scope.url;
            var jobportal=$("#jobportal").val() ;
            var status = $scope.workstat
            var isinvite=$scope.isinvite
            var comments=$scope.comments
            var parser = document.createElement('a');
            parser.href = joburl;
            if (!isinvite){
                isinvite = 'no';
            }
            else{
                isinvite='yes'
            }
    if (biddername && joburl && status){
        var url=joburl;
        var rurl= /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;  
        if ((rurl.test(url))){
            var JobId =  getid(url);  
            var newbids={
                BidderName: biddername,
                JobUrl: joburl,
                protocol:parser.protocol,
                Host:parser.host,
                Hostname: parser.hostname,
                hash:parser.hash,
                search:parser.search,
                path:parser.pathname,
                parameters:parser.search,
                JobId: JobId,
                JobPortal: jobportal,
                Status: status,
                Isinvite: isinvite,
                Comments: comments 
            };
            console.log(newbids);
        $http({method: 'POST', url: '/bidsave',data:newbids}).
            success(function(data, status, headers, config) {
                console.log(data);
                $scope.biddata=data;
                $(".success-msg").show();
                $('success-msg').delay(4000).fadeOut();
                $('#BidForm').each(function(){
                    this.reset();   //Here form fields will be cleared.
                });
            }).
            error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });       
        }
    else{
        $(".url-error").show();
        $('.url-error').delay(4000).fadeOut();
        return false;
    }
    }
    else {
        if (!biddername){
            $(".name-error").show();
            $('.name-error').delay(4000).fadeOut();
        }
        if (!joburl){
            $(".urlempty-error").show();
            $('.urlempty-error').delay(4000).fadeOut();
            return false;
        }
        if (!status){
            $(".status-error").show();
            $('.status-error').delay(4000).fadeOut();
            return false;
        }
    }

};
   
};


// Controller for  bids 
function Searchbid($scope,$http,$cookies,$location,$rootScope,$route) {



//to hide the bid info on clicking anywhere on page
$(document).click(function() {
    $(".bdr-dtl-fld").hide();
});
$(".bdr-dtl-fld").click(function(e) {
    e.stopPropagation();
    return false;       
                        
});



//
    var user= $.cookie("cookiename");
     if (user){
        $rootScope.user = "done" ;
        $location.url('/searchbid');
     }
     else{
         $location.url('/login');
     }
    var cookie={
            cookiename : user
            };
    $http({method: 'POST', url: '/latestbids',data:cookie}).
    success(function(data, status, headers, config) {
        //console.log(data.bids,"this data works");
        //show 'today' instead of today's date
        var len = data.bids.length;
        //var date = data.bids[0].date;
        var today = new Date();
        var pdate = today.getDate();
        var pyear = today.getFullYear();
        var pmonth = today.getMonth();
        var prdate =("" + pdate + pmonth + pyear);
        for(var i = 0;i<len;i++){
            var d = data.bids[i].date;
            var dt = new Date(d)
            var dbdate = dt.getDate();
            var dbyear = dt.getFullYear();
            var dbmonth = dt.getMonth();
            var dbsdate =("" + dbdate + dbmonth + dbyear);
            // console.log(prdate,"today");
            // console.log(dbsdate,"dates");
            if (prdate == dbsdate){
                $scope.create = "Today";
                data.bids[i].date = $scope.create;
            }
            else{
                $scope.create = data.bids[i].date[8]+data.bids[i].date[9]+" "+data.bids[i].date[4]+
                data.bids[i].date[5]+data.bids[i].date[6];
                data.bids[i].date = $scope.create;
            }
        }

        //to get date in leads section
        for(var i = 0;i<len;i++){
            var d = data.bids[i].Modifieddate;
            var leaddt = new Date(d)
            var leaddbdate = leaddt.getDate();
            var leaddbyear = leaddt.getFullYear();
            var leaddbmonth = leaddt.getMonth();
            var leaddbsdate =("" + leaddbdate + leaddbmonth + leaddbyear);
            // console.log(prdate,"today");
            // console.log(dbsdate,"dates");
            if (prdate == leaddbsdate){
                $scope.modify = "Today";
                data.bids[i].Modifieddate = $scope.modify;
            }
            else{
                $scope.modify = data.bids[i].Modifieddate[8]+data.bids[i].Modifieddate[9]+" "+data.bids[i].Modifieddate[4]+
                data.bids[i].Modifieddate[5]+data.bids[i].Modifieddate[6];
                data.bids[i].Modifieddate = $scope.modify;
            }
        }


        //
        $scope.allbids = data; 
        //console.log($scope.allbids);
        var newArray = data.bids.slice();
        //console.log("newArray", newArray);

        newArray.sort(function(a,b){
            return b.Count-a.Count;
        })
         $scope.latestdata = newArray;
    });


    // Clicking on Search button calls this function to search bids
    $scope.findbid= function() {
        $("#checkuser").hide();
        $(".new-msg").hide();
        $(".new-msg").hide();
	$("#bidinfo").hide();
        var joburl=$scope.joburl
        var url=joburl;
        var rurl= /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;  
        if ((rurl.test(url))){
          var JobId =  getid(url);          
            if (JobId){
                var bids={
                    Joburl: joburl,
                    JobId: JobId,
                };
            }
            else{
                var check = url.split('/');
                var len =  check.length;
                var last =  (check[len-1]);
                var slast = (check[len-2]);
                if (slast == "applications"){
                    $(".app-msg").show();
                    $('.app-msg').delay(2000).fadeOut();
                    return false;
                }
            }
        
  
        //to get bids data using job url and job id
        $http({method: 'POST', url: '/bidgetsearch',data:bids}).
            success(function(data, status, headers, config) {
                $scope.bidsinfo = data.bids;
                //console.log($scope.bidsinfo[0].bidtype,"ssfsdsf");
                if ($scope.bidsinfo != ''){
                    $scope.st = $scope.bidsinfo[0].Status
                    if ($scope.st == "Applied"){
                        $scope.st = "Applying"
                        $scope.s = "Applied";
                        $("#change-btn").addClass('applied');
                    }
                    else{
                        $scope.st = "Applied"
                        $scope.s = "Applying";
                    }
                    var username = $cookies.cookiename
                    if ($scope.bidsinfo[0].BidderName == username){
                        if($scope.bidsinfo[0].bidtype == 'lead'){
                        $("#leadinf").show();
                
                    }else{
                        $("#checkuser").show();
                        $("#leadinf").hide();
                    }
                }
                    else{
                        $(".already-msg").show();
                        $('.already-msg').delay(4000).fadeOut();
                        $("#leadinf").hide();
                    }
                
            }
                else{
                    $(".new-msg").show();
                    $(".go-btn").show();
                    $("#leadinf").hide();
                }

            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });       
        }
        else{
            $(".invalid-msg").show();
            $('.invalid-msg').delay(2000).fadeOut();
            return false; 
        }
    }
    // to show the section for applying for the job
    $scope.go= function() {
        $("#check").show();
        $(".new-msg").hide();
    }

    //view bids information
    $scope.viewbid= function(even) {
        var joburl = $scope.joburl;
        if(typeof even == 'undefined') {
	    var JobId =  getid(joburl);
            var view = "form";
        }else{
          var view = "latest";
 	  var JobId = $("."+ even.target.id).text();
	  //console.log($("#"+ even.target.id).next().next().next());        
		}
 
        var bids={
            JobId: JobId,
        };
        $http({method: 'POST', url: '/bidgetsearch',data:bids}).
        success(function(data, status, headers, config) {
            console.log(data.bids.length,"asdsads");
            $scope.info=data;
            var name1 = data.bids[0].BidderName;
	   var nm = name1.replace('.',' ');
	    $scope.name = nm;
            if (data.bids.length == 1){
                if(view == 'form'){
                    $('#bidinfo').show();
                    $('#info').hide();
                }else{
		    $scope.info=data;
                    //console.log($("."+ even.target.id).parent().next().next().text());
                   var s=   $("."+ even.target.id).parent().next().next().after($('#info').show());
			console.log(s.next());
                    $('#bidinfo').hide();
                }
            }
        });
    }   


    //save values to database using the search form
    $scope.save= function() {
        var joburl = $scope.joburl;
        //var JobId =  getid(joburl); 
        var contains = (joburl.indexOf('odesk') > -1); //true
        if (contains){
            var JobId = joburl.split('~');
            var id = JobId[1]
            var check1 =   (id.indexOf('?') > -1); //true
            var check2 =    (id.indexOf('/') > -1); //true
            if (check1){
                var Id = id.split('?');
                var JobId = Id[0];
            }
            if (check2){
                var Id = id.split('/');
                var JobId = Id[0];
            }
            else {
                var JobId = JobId[1];
            }
            JobPortal="odesk";
        }
        else if (!contains) {
            var r = /\/\d+\//;
            var s = joburl;
            var Id = s.match(r);
            var sid = String(Id);
            var JobId = sid.replace(/\//g,'');
            JobId = JobId;
            JobPortal="elance"
        }
        status="Applying"
        var parser = document.createElement('a');
        parser.href = joburl;
        isinvite=$scope.invite;
        if (!isinvite){
            isinvite = 'no';
        }
        else{
            isinvite='yes'
        }
        var newbids={
            BidderName:$cookies.cookiename,
            JobUrl: joburl,
            protocol:parser.protocol,
            Host:parser.host,
            Hostname: parser.hostname,
            hash:parser.hash,
            search:parser.search,
            path:parser.pathname,
            parameters:parser.search,
            JobId: JobId,
            JobPortal: JobPortal,
            Status: status,
            Isinvite: isinvite, 

        };
        console.log(newbids,"newbids");
        $http({method: 'POST', url: '/bidsave',data:newbids}).
        success(function(data, status, headers, config) {
            $scope.biddata=data;
            //$(".saved-msg").show();
            //$('.saved-msg').delay(1000).fadeOut();
            $(".new-msg").hide();
            $("#check").hide();
            $('#searchbid').each(function(){
                this.reset();   //Here form fields will be cleared.
            });
            var user= $.cookie("cookiename");
            var cookie={
                cookiename : user
            };
            $http({method: 'POST', url: '/latestbids',data:cookie}).
            success(function(data, status, headers, config) {
                console.log(data.bids,"check data");
                $scope.allbids = data;

		$route.reload();
		
		
            });
        }).
        error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        });        


    }
 

    //change status for a particular job
    $scope.change= function(even) {
    var joburl = $scope.joburl;
    $('#bidinfo').hide();
    if (typeof even == 'undefined'){
        var JobId =  getid(joburl);
    }
    else{
        var JobId = $("."+ even.target.id).text();
        $('#info').hide();
    }
    var data = $scope.joburl
    if (!data){
        var a = $("#"+ even.target.id).next().next().next().after();
        $('.stat-msg').delay(2000).fadeOut();
        $('#info').hide();
    }
    var bids={
            JobId: JobId,

            };
    $http({method: 'POST', url: '/bidgetsearch',data:bids}).
        success(function(data, status, headers, config) {
            var stat = data.bids[0].Status;
            var bids={
                JobId: JobId,
                status: stat
            };
            $http({method: 'POST', url: '/changestatus',data:bids}).
                success(function(data, status, headers, config) {
                $scope.fromstatus = stat;
                if (typeof even == 'undefined'){
                    if(stat == "Applying"){
                        $scope.st = "Applied";
                        $scope.s = "Applied";
                        $(".job-lead-1").addClass('applied');
                        $(".inprgs Applying").addClass('applied');
                    }else{
                        $scope.s = "Applying";
                        $scope.st = "Applying";
                        $(".job-lead-1").removeClass('applied');
                    
                    }
                    
                }
                else{
                if(stat == "Applying"){
                        $scope.st = "Applied";
                        //$scope.s = "Applied";
                        console.log( $("."+ even.target.id).parent().next().children('a').eq(2));
                        $("."+ even.target.id).parent().next().children('a').eq(2).addClass('applied');

                        
                }else{
                        $scope.st = "Applying";
                        //$scope.s = "Applying";
                        $("."+ even.target.id).parent().next().children('a').eq(2).removeClass('applied');
                        $("."+ even.target.id).parent().next().children('a').eq(2).removeClass('.inprgs Applied applied');
                        //$("#"+ even.target.id).next().attr("title", $scope.st);
                        
                    }
                        
                }
                    $('#bidinfo').hide();
                    //$route.reload();


                }).
                error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
                });      

        }).
        error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });        

    }



    // delete a bid from database
    $scope.delete= function(even) {
    var cont = confirm('Are you sure ?');
    if (cont){
        var joburl = $scope.joburl;
        if (typeof even == 'undefined'){
            var JobId =  getid(joburl);
        }
        else{
            var JobId = $("."+ even.target.id).text();
            $("#"+ even.target.id).parent().parent().remove();
        }
        var data = $scope.joburl
        if (!data){
            var a = $("#"+ even.target.id).next().next().next().after("<span style='color:green'  class='remove-msg' >Data Deleated</span><br>");
            $("#"+ even.target.id).parent().parent().remove();
            $('.remove-msg').delay(2000).fadeOut();
            $('#info').hide();
        }
        var bids={
            JobId: JobId,

            };
        console.log(bids);
        $http({method: 'POST', url: '/bidgetsearch',data:bids}).
        success(function(data, status, headers, config) {
            //var stat = data.bids[0].Status;

            $http({method: 'GET', url: '/deletebid?data='+JobId }).
            success(function(data, status, headers, config) {
                console.log(data);
                $(".delete-msg").show();
                $('.delete-msg').delay(2000).fadeOut();
                $("#bidinfo").hide();
            }).
            error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });      

        }).
        error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });        

    }
}

//change to lead
$scope.lead= function(even,param1) {   
    $("#lead").hide();
    $("#mdate").hide();
    $('#myModal').modal('toggle');
        $("#savebtn").show();
    $("#editbtn").hide();
//autocomplete for the skills required.
    // $("#skillKeyWords").tokenInput('/searchautocomplete', {
    //         theme: "facebook",
    //         onResult: function (results) {   
    //             console.log(results);
    //             return results ;                
    //         },
    //         onAdd: function(item)
    //         {
    //          $("#hdskillkey").val(item.name);
    //         }
    //     });



    
    $scope.title = '';
            $scope.appurl = '';
            $scope.msgurl = '';
            $scope.interviews = '';
            $scope.cname = '';
            $scope.cemail = '';
            $scope.budget = '';
            $scope.skype = '';
            $scope.hist = '';
            $scope.leadtype = '';
            $scope.stat = '';
            $scope.bidamount  = '';

    if (typeof even != 'undefined'){
        $scope.hid =  $("."+ even.target.id).text();
        var url = $("."+ even.target.id).next().next().text();
    }else{
        var url = $scope.joburl;
    }
    var containsodesk = (url.indexOf('odesk') > -1);
    if (containsodesk){
        $scope.port = "Odesk";
    }else{
        $scope.port = "Elance";
    }
    $scope.jurl = url;
}

//save lead data
$scope.leadsave = function(){
    var url = $scope.joburl;
    if (url){
        var JobId =  getid(url);
    }
    else{
        var JobId = $scope.hid;;
    }
    

    var title = $scope.title;
    var appurl = $scope.appurl;
    var msgurl = $scope.msgurl;
    var interviews = $scope.interviews;
    var leadtype = $scope.leadtype;
    var cname = $scope.cname;
    var cemail = $scope.cemail;
    var budget = $scope.budget;
    var skype = $scope.skype;
    var history = $scope.hist;
    var stat = $scope.stat;
    var bidamount = $scope.bidamount;
    var skills=$scope.skills;


//validation for the form
if(title && appurl && interviews && leadtype && cname && budget && history && stat && bidamount){
    var vldurl = validurl(appurl);
        if(msgurl){
            var valid = validurl(msgurl);
            if (valid == 2){
                $(".msg-error").show();
                $(".msg-error").delay(4000).fadeOut();
                return false;
            }
        }
        if(cemail){
            var mail = validemail(cemail);
            if (mail == 2){
                $(".em-error").show();
                $(".em-error").delay(4000).fadeOut();
                return false;
            }
        }

    if (vldurl == 1){
    var newlead={
                Title: title,
                ApplicationUrl: appurl,
                MessageUrl:msgurl,
                Interviews:interviews,
                leadtype:leadtype,
                ClientName:cname,
                ClientEmail:cemail,
                Clientbudget:budget,
                ClientSkype:skype,
                Clienthistory: history,
                CurrentStatus:stat,
                JobId : JobId,
                BidAmount:bidamount,
                Skills:skills
    };

    var  newclient = {
        ClientName:cname,
        ClientEmail:cemail,
        Clientbudget:budget,
        ClientSkype:skype,
        Clienthistory: history
    }
    //console.log(newlead,"here we are");
    var bidtype={
            bidtype: 'lead',
            }; 

$http({method: 'POST', url: '/leadcount',data:bidtype}).
        success(function(data, status, headers, config) {
            console.log(data,"this is bidtype lead");
            var count = data.bids.length;
            var bids={
                JobId: JobId,
                count : count,

            }; 
            //console.log(bids,"count");
            $http({method: 'POST', url: '/changebidtype',data:bids}).
            success(function(data, status, headers, config) {
                console.log(data,"bid type and count changed");

                $http({method: 'POST', url: '/clientsave',data:newclient}).
                success(function(data, status, headers, config) {
                    console.log(data);
                    $http({method: 'POST', url: '/leadsave',data:newlead}).
                    success(function(data, status, headers, config) {
                        console.log(data);
                    }).

                    error(function(data, status, headers, config) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                    }); 
                }).
                error(function(data, status, headers, config) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                });        

            }).
            error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            }); 
                $('#myModal').modal('hide')  
               $route.reload();
        }).
        error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });       
                
        }else{
            $(".valid-error").show();
            $(".valid-error").delay(4000).fadeOut();
        }

}
else{
    errors = {'title-error':title,'app-error':appurl,'int-error':interviews,'bid-error':bidamount,'lead-error':leadtype,
    'name-error':cname,'budget-error':budget,'hist-error':history, 'stat-error':stat};
     console.log(errors);
    $.each( errors, function( key, value ){
        //console.log(value);
        if(value == ""){
            $("."+key).show();
            $("."+key).delay(4000).fadeOut();
        }
    });
}
}


//edit leads info 
$scope.editleads= function(even) {

var url = $scope.joburl;
    if (url){
        var JobId =  getid(url);
    }
    else{
        var JobId = $scope.hid;
    }
    var title = $scope.title;
    var appurl = $scope.appurl;
    var msgurl = $scope.msgurl;
    var interviews = $scope.interviews;
    var leadtype = $scope.leadtype;
    var cname = $scope.cname;
    var cemail = $scope.cemail;
    var budget = $scope.budget;
    var skype = $scope.skype;
    var history = $scope.hist;
    var stat = $scope.stat;
    var bidamount = $scope.bidamount;
    var skills=$scope.skills;
	
var nlead={
                Title: title,
                ApplicationUrl: appurl,
                MessageUrl:msgurl,
                Interviews:interviews,
                leadtype:leadtype,
                ClientName:cname,
                ClientEmail:cemail,
                Clientbudget:budget,
                ClientSkype:skype,
                Clienthistory: history,
                CurrentStatus:stat,
                JobId : JobId,
                BidAmount : bidamount,
                Skills : skills
    };

$http({method: 'POST', url: '/editlead',data:nlead}).
            success(function(data, status, headers, config) {
                console.log(data,"here it is");
                $('#myModal').modal('hide');

            }).
            error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });      


}






//Get lead info to show in the form
$scope.leadinfo= function(even) {
$('#myModal').modal('toggle')   
    $("#savebtn").hide();
    $("#editbtn").show();
var h = $('#hidbtn').val();
$scope.hidbtn = h;
//var u = $("#"+ even.target.id).parent().parent().children();
//console.log(u);
var url = $scope.joburl;
if (url){
    var id =  getid(url);
}else{
    id = $("#"+ even.target.id).attr('data-cid');
}
$scope.hid = $("#"+ even.target.id).attr('data-cid');
 var bids={
            JobId: id,

            }; 

$http({method: 'POST', url: '/bidgetsearch',data:bids}).
        success(function(data, status, headers, config) {
            console.log(data,"here is")
            $scope.port = data.bids[0].JobPortal;
            $scope.jurl = data.bids[0].JobUrl;
            $scope.leadId = data.bids[0].Count;
            var m = data.bids[0].Modifieddate;
            $scope.mdate=(m[8]+m[9]+" "+m[4]+m[5]+m[6]+" "+m[11]+m[12]+m[13]+m[14]);
            }).
            error(function(data, status, headers, config) {
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });      




$http({method: 'POST', url: '/leadinfo',data:bids}).
        success(function(data, status, headers, config) {
            //console.log(data.leads[0],"ssdsdvgdfhgfhfcscf");
            $scope.title = data.leads[0].Title;
            $scope.appurl = data.leads[0].ApplicationUrl;
            $scope.msgurl = data.leads[0].MessageUrl;
            $scope.interviews = data.leads[0].Interviews;
            $scope.cname = data.leads[0].ClientName;
            $scope.cemail = data.leads[0].ClientEmail;
            $scope.budget = data.leads[0].Clientbudget;
            $scope.skype = data.leads[0].ClientSkype;
            $scope.hist = data.leads[0].Clienthistory;
            $scope.leadtype = data.leads[0].leadtype;
            $scope.stat = data.leads[0].CurrentStatus;
            $scope.bidamount  = data.leads[0].BidAmount;
            $scope.skills  = data.leads[0].Skills;
              
        }).
        error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
        });        
}
};

// check login details 
function loginCtrl($scope,$http,$rootScope,$location,$cookies){
var user = $.cookie("cookiename");
  $scope.userlogin = function() {
    var username = $scope.username;
    var password = $scope.password;
    var user={
            username: username,
            password: password,
            };

    $http({method: 'POST', url: '/checklogin',data:user}).
    success(function(data, status, headers, config) {
        //console.log(data.name[0].Username,"dataf");

        if (data){
            $rootScope.user = "done" ;
            $.cookie("cookiename", data.name[0].Username,{ expires: 7 });
            $location.url('/searchbid');  
              
        }
        else{
            $(".error-msg").show();
            $('.error-msg').delay(2000).fadeOut();
        }
    }).
    error(function(data, status, headers, config) {
        $(".error-msg").show();
        $('.error-msg').delay(2000).fadeOut()
        // called asynchronously if an error occurs
        // or server returns response with an error status.
    });


  };

//logout
$scope.logout = function() {
var cookies = $.cookie();
$.removeCookie("cookiename");
}
};

// function to Get JobId from JobUrl 
function getid(value) {
var tilt = value.split('~');
var splitslash = tilt[0].split('/');
var test = (splitslash[2])

if(test == "www.odesk.com"){
    var check = value.split('/');
    var len =  check.length;
    var last =  (check[len-1]);
    var slast = (check[len-2]);
    if (slast == "applications"){
        $(".app-msg").show();
        $('.app-msg').delay(2000).fadeOut();
        return false;
    }
    var JobId = value.split('~');
            var id = JobId[1]
            var check1 =   (id.indexOf('?') > -1); //true
            var check2 =    (id.indexOf('/') > -1); //true
            if (check1){
                var Id = id.split('?');
                var JobId = Id[0];
            }
            if (check2){
                var Id = id.split('/');
                var JobId = Id[0];
            }
            else {
                var JobId = JobId[1];
            }

}
else if (test == "www.elance.com"){
        var regex = /\/\d+\//;
            var url = value;
            var exist = url.match(regex);
            var strid = String(exist);
            var JobId = strid.replace(/\//g,'');
            JobId = JobId;
}

return JobId;
}
//ends here
   
//controller for deleting a project
function projectCtrl($scope,$http,$route){
    $scope.del= function() {
        var pro = $scope.user;
        var cont = confirm('Are you sure ?');
        if (cont){
            $http({method: 'GET', url: "/deleteproject?key="+pro}).
            success(function(data, status, headers, config) {
                console.log(data.status);
               if(data.status == 1){
                location.reload();
                }
            }).
            error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            });  
        }
    }
}

//function for validating a url
function validurl(value) {
var url=value;
var rurl= /^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\%"\,\{\}\\|\\\^\[\]`]+)?$/;  
if ((rurl.test(url))){
    var r = 1;
}else{
    var r = 2;
}
return r;
}


//function for valid ClientEmail 
function validemail(value) {
var email=value;
var remail = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
if ((remail.test(email))){
    var em = 1;
}else{
    var em = 2;
}
return em;
}

function dateformat(data,type){

var len = data.length;
        //var date = data.bids[0].date;
        var today = new Date();
        var pdate = today.getDate();
        var pyear = today.getFullYear();
        var pmonth = today.getMonth();
        var prdate =("" + pdate + pmonth + pyear);
        for(var i = 0;i<len;i++){
            if (type == "created"){
                var d = data[i].date;
            }
            else{
                var d = data[i].Modifieddate;
            }
            
            var dt = new Date(d)
            var dbdate = dt.getDate();
            var dbyear = dt.getFullYear();
            var dbmonth = dt.getMonth();
            var dbsdate =("" + dbdate + dbmonth + dbyear);
            // console.log(prdate,"today");
            // console.log(dbsdate,"dates");
            if (prdate == dbsdate){
                $scope.create = "Today";
                d = $scope.create;
            }
            else{
                $scope.create = d[8]+d[9]+" "+d[4]+
                d[5]+d[6];
                d = $scope.create;
            }
        }


}
