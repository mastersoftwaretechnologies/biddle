// Angular module, defining routes for the app
var app = angular.module('biddle',["ngCookies"]);
	app.config(['$routeProvider',"$cookiesProvider", function($routeProvider,$cookies) {
		$routeProvider.
			when('/newbid', { templateUrl: '../partials/newbidform.html',controller: bidCtrl }).
			when('/searchbid', { templateUrl: '../partials/searchbid.html',controller:Searchbid }).
			when('/login', { templateUrl: '../partials/login.html',controller: loginCtrl }).
			// If invalid route, just redirect to the main list view
			otherwise({ redirectTo: '/login' });
	}]);

app.directive('ngBlur', function() {
  return function( scope, elem, attrs ) {
    elem.bind('blur', function() {
      scope.$apply(attrs.ngBlur);
    });
  };
});

//autofill the username, password fields and pass values to ng-model
app.directive('autoFillSync', function($timeout) {
   return {
      require: 'ngModel',
      link: function(scope, elem, attrs, ngModel) {
          var origVal = elem.val();
          $timeout(function () {
              var newVal = elem.val();
              if(ngModel.$pristine && origVal !== newVal) {
                  ngModel.$setViewValue(newVal);
              }
          }, 500);
      }
   }
});

