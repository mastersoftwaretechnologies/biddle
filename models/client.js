var mongoose = require('mongoose');
var db;
if (process.env.VCAP_SERVICES) {
   var env = JSON.parse(process.env.VCAP_SERVICES);
   db = mongoose.createConnection(env['mongodb-2.2'][0].credentials.url);
} else {
   db = mongoose.createConnection('localhost', 'nodetest1');

}
// Document schema for client info
//console.log("here", new Date());
exports.clientSchema = new mongoose.Schema({
	ClientName : { type: String },
	ClientEmail : { type: String },
	Clientbudget : { type: Number },
	ClientSkype :{ type: String },
	Clienthistory : { type: String },
});