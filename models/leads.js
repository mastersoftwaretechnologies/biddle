var mongoose = require('mongoose');
var db;
if (process.env.VCAP_SERVICES) {
   var env = JSON.parse(process.env.VCAP_SERVICES);
   db = mongoose.createConnection(env['mongodb-2.2'][0].credentials.url);
} else {
   db = mongoose.createConnection('localhost', 'nodetest1');

}

exports.leadsSchema = new mongoose.Schema({
	Title : { type: String },
	ApplicationUrl : { type: String },
	MessageUrl : { type: String },
	Interviews : { type: Number },
	leadtype : { type: String },
	ClientName : { type: String },
	ClientEmail : { type: String },
	Clientbudget : { type: Number },
	ClientSkype :{ type: String },
	Clienthistory : { type: String },
	CurrentStatus : { type: String },
	JobId:{ type: String },
	date: { type: String, default: Date() },
	BidAmount: { type: Number },
	Skills: { type: String }
});


