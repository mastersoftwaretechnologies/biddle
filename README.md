##Biddle
======
Bidding Management Software helps to expedite bidding on portals like odesk and Elance.


## Stack
* [nodejs](http://www.nodejs.org)
* [Bootstrap3](http://getbootstrap.com/)
* [AngularJs](https://angularjs.org/‎)
* [ExpressFramework](expressjs.com/)

##Databases
Mongodb
MySql

##Setup
Biddle Setup on local Machine:
Steps to follow to setup biddle on your machine:
	1)Install Dependencies: 
		(i)Install node js.
		(ii)Install express framework.
		(iii)Install node js required modules.
			mysql: npm install mysql
			mongoose: npm install mongoose
			passport: npm install passport
			passport-local : npm install passport-local
			socket.io: npm install socket.io

	2)Create a folder where you wish to setup the project.

	3)Go to that folder using command prompt and Download latest code from bitbucket repo using command : 
		git clone https://mastersoftwaretechnologies@bitbucket.org/mastersoftwaretechnologies/biddle.git

	4)There will be a folder created  named Biddle.

	5)Change permissions if required.(sudo chmod 0777 -R foldername)

	6)Import the database to your local machine.


	7)cd to biddle folder and start the app using : node app.js

You can now see running instance of Biddle in browser at http://localhost:3002.


##Mongodb backup on the local machine :
		(reference :http://www.tutorialspoint.com/mongodb/mongodb_create_backup.htm)
1)Connect to server

2)cd to root directory

3)To backup a particular collection from a database:use the following command
	mongodump --collection mycol --db test
	where
	mycol- name of the collection you wish to backup
	test- database name.

To backup the whole db:
		mongodump --host HOST_NAME --port PORT_NUMBER

##Convert BSON TO JSON
bsondump collection.bson > collection.json
